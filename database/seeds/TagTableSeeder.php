<?php

use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $q = [['tag'=>'new-arrival'],['tag'=>'best-seller'],['tag'=>'mgc-product']];

        for ($i=0; $i < count($q); $i++) { 
        	\DB::table('tag')->insert($q[$i]);
        }
    }
}
