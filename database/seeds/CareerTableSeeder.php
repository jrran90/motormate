<?php

use App\Career;
use Illuminate\Database\Seeder;

class CareerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // remove this after
    	$arr = [
    		[
    			'job_title'=>'Motorcycle C.I./Collector',
    			'details'=>'<ul>
			                    <li>Must be Male</li>
			                    <li>Not more than 30 years old</li>
			                    <li>A graduate of any four year course</li>
			                    <li>Experienced in credit and collection is an advantage</li>
			                    <li>Excellent in written and verbal communication</li>
			                    <li>Must have a drivers license</li>
			                    <li>Must be honest, flexible and hardworking person</li>
			                    <li>Willing to travel</li>
			                </ul>',
			    'employment_type'=>'full-time',
			    'ended_at'=>'2015-12-31'
    		],
    		[
    			'job_title'=>'Sales Representative',
    			'details'=>'<ul>
			                    <li>Male or Female</li>
			                    <li>Not more than 28 years old</li>
			                    <li>A graduate of four year course, preferably business course</li>
			                    <li>Previous sales experience is an advantage</li>
			                    <li>Excellent communication skills both oral and written with strong listening skills</li>
			                    <li>Must be aggressive, highly competitive & self-motivated</li>
			                    <li>Has initiative, responsible and willing to work long hours</li>
			                </ul>',
			    'employment_type'=>'full-time',
			    'ended_at'=>'2015-12-31'
    		],
            [
                'job_title'=>'Company Driver/Mechanic',
                'details'=>'<ul>
                                <li>Male, 25-35 years old</li>
                                <li>Preferably with experience of any vehicle(s) or delivery van</li>
                                <li>Must be honest, flexible and hardworking person</li>
                                <li>Preferably with high driver\'s license restriction code</li>
                                <li>Can fix and maintain vehicle\'s condition (mechanic)</li>
                                <li>Willing to travel</li>
                            </ul>',
                'employment_type'=>'full-time',
                'ended_at'=>'2015-12-31'
            ],
            [
                'job_title'=>'Branch Cashier',
                'details'=>'<ul>
                                <li>Female, must be single</li>
                                <li>Not more than 28 years old</li>
                                <li>Graduate of any four year business course</li>
                                <li>Willing to be assigned at any of our provincial branches</li>
                                <li>Preferably with experience in  Cashiering and Finance related job</li>
                                <li>Must be honest, flexible and a hardworking</li>
                            </ul>',
                'employment_type'=>'full-time',
                'ended_at'=>'2015-12-31'
            ],
            [
                'job_title'=>'Service Crew',
                'details'=>'<ul>
                                <li>Male of Female</li>
                                <li>18-25 years old preferably single</li>
                                <li>Good in customer relations and with pleasing personality</li>
                                <li>Must be honest, flexible and hardworking person</li>
                                <li>Must be willing to work overtime when needed and can work under pressure</li>
                            </ul>',
                'employment_type'=>'full-time',
                'ended_at'=>'2015-12-31'
            ],
            [
                'job_title'=>'Management Trainee',
                'details'=>'<ul>
                                <li>Male/Female, at least 25 years of age</li>
                                <li>A graduate of any four (4) year business course</li>
                                <li>With not less than two (2) years experience, preferably with experience in supervisory position</li>
                                <li>Masters in Business Administration is an advantage but not a requirement</li>
                            </ul>',
                'employment_type'=>'full-time',
                'ended_at'=>'2015-12-31'
            ],


    	];

    	for ($i=0; $i<count($arr); $i++) {
			Career::create($arr[$i]);
    	}
    }
}
