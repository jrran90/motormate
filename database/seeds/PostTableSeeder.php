<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $q = [
        		[
        			'user_id'	=> 1,
        			'title'		=> 'MGC Promos',
        		    'content'	=> '<h5 style="font-weight: bold;color:#000">Free change oil every 7-8am and 5-6pm</h5><p>Motormate service center is giving a Free change oil service every 7-8am and 5-6pm daily,So bring your motorcycle now and grab this promo. This promo is until december only.</p>
        		                <h5 style="font-weight: bold;color:#000">Free installation on selected mgc brands accessories</h5><p>Purchase of any selected MGC brands item and we will install it to your motorcycle for free. So what are you waiting?  purchase now and avail free installation.  For more information just approach our friendly staff. This promo is until december only.</p>
        		                <h5 style="font-weight: bold;color:#000">Upto 25% discount selected accessories</h5><p>Motormate is now giving 25% discount on selected accessories, so hurry and purchase your accessories with discounted price. This promo is until december only.</p>
        		                <h5 style="font-weight: bold;color:#000">Free installation on selected accessories</h5><p>Purchase of any selected accessories  and we will install it to your motorcycle for free. So what are you waiting?  purchase now and avail free installation.  For more information just approach our friendly staff. This promo is until december only.</p>',
        		    'image'		=> 'promo.jpg'
        		],
        		[
        			'user_id'	=> 1,
        			'title'		=> '3 Way Giveaway',
        		    'content'	=> '<h5 style="font-weight: bold;color:#000">MIO SPORTY 3 WAY GIVEAWAY PROMO MECHANICS</h5><p>Promo is open to customers who purchase a brand new Yamaha    Mio Sporty unit on cash or installment basis.</p><p>Customers who will purchase a unit of Yamaha Mio Sporty within the promo period will automatically receive one (1) Jacket,  one (1) Bag and one (1) T-shirt packed in an eco-bag.</p><p>There are no limitations on how many Yamaha Mio Sporty units a customer can buy.</p><p>Free items can be redeemed on the date of purchase or within 60 days from the end of the promo period at the store where the purchase was made.</p><p>Promo is open to all Yamaha 3S Shops and multi-brand outlets nationwide.</p><p>Promo period is from October 15, 2015 to January 15, 2016.</p><p>Per DTI FTEB SPD Permit no.: 12741 Series of 2015</p>
                <h5 style="font-weight: bold;color:#000">RS PAMPASADA PROMO MECHANICS</h5>
                <ul><li>Promo is open to all customers who purchase a brand new Yamaha RS 110F unit on cash or regular installment basis. Yamaha RS 110F is available nationwide</li><li>Upon purchase of a Yamaha RS 110F unit, the customer will get one (1) Denim Jacket one (1) Jersey and one (1) Leg Bag.</li><li>Period of redemption/Claim period: Upon purchase of Yamaha RS 110F motorcycle or until 60 days from end of promo period.</li><li>Promo is open to all Two Hundred Ten (210) Yamaha 3S Shop Outlets (exclusive) as well as One Thousand Four Hundred Ninety Nine (1,499) Multibrand Shop Outlets Nationwide.</li><li>There are no limitations on how many RS 110F units a customer can buy.</li><li>There are no limitations on how many RS 110F units a customer can buy.</li><li>Promo period is from October 15, 2015 to January 15, 2016.</li></ul><p>Per DTI FTEB SPD Permit no.: 12743 Series of 2015 Series of 2015</p>',
        		    'image'		=> 'promo1.png'
        		],
        		[
        			'user_id'	=> 1,
        			'title'		=> 'Christmas Surprise Promo',
        		    'content'	=> '<p>Promo is open to customers who purchase any of the following unit on cash or installment basis:</p><ul><li>Mio i 125</li><li>Mio Soul i 125</li><li>Mio MXi 125</li><li>SZ</li></ul><p>Employee of Yamaha Motor Philippines Inc. (YMPH) and 3s Shops, including their relatives up to the second degree of consanguinity or affinity are disqualified to join the promotion.</p><p>Upon purchase of any of these units, the customer will pick one greeting card which may contain any of the following prizes (there will be no non-winning card)</p><ul><li>G shock watch</li><li>Smart phone O+</li><li>JBL Speaker</li><li>Sony Headphone</li><li><strong>Grand raffle prize:</strong> Sony  LED TV with Home Theater</li><li>The number of card that a customer can avail depends on the number of motorcycles they purchase within the promo period. One motorcyle is equivalent to one greeting card, the prize indicated in the picked greeting card shall be given immediately to the winner.</li><li>To join this raffle customers must fill up the assigned portion of the greeting card with their name, address, contact number, signature and name of the purchased motorcycle unit for the chance to win one (1) Sony LED TV with Home Theater. Drop it in a dropped boxes located at the branch of purchase.</li><li>Raffle entries will be drawn at Yamaha Sales and Marketing Office in Ortigas Center, Pasig City in the presence of DTI Representative. It will be conducted on Feb. 19, 2016. There will be one winner per Yamaha 3S Shop, a total of Two Hundred Forty Five (245) Sony LED TV\'s with Home Theater will be given away.</li><li>Winners will be announced on Yamaha\'s Official Website - www.yamaha-motor.com.ph and will be notified through their contact numbers and their registered mail.</li><li>Winner should present their claiming stub and two (2) valid IDs at the Yamaha 3S shop where they purchase their motorcyle unit. They can claim their prizes within 60 days from reciept of notification.</li><li>Deadline of submission of entries is on Febuary 5, 2016.</li><li>Yamaha Motor Philippines shall pay 20% Tax for prizes exceeding Php 10,000.00</li><li>Promo period from November 1, 2015 tp January 31, 2016.</li><li>Promo is open to Two Hundred Forty Five (245) Yamaha 3S outlets nationwide</li><li>Prizes not claimed within 60 days from reciept of notification will be forfeited with prior approval of DTI.</li></ul>',
        		    'image'		=> 'promo-2.png'
        		],
        	];

        $destinationPath  = public_path().'/assets/img/slideshows/thumb/';

        for ($i=0; $i < count($q); $i++) { 
            Post::create($q[$i]);
        }        

    }
}
