<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$arr_users = [
    		['username'=>'jrran90','password'=>bcrypt('1234'),'email'=>'jr.ran90@gmail.com','display_name'=>'jhon','is_admin'=>1,'hash'=>md5(time().mt_rand(10,100)),'is_active'=>'1'],
    		['username'=>'editor','password'=>bcrypt('editor'),'email'=>'user@gmail.com','display_name'=>'editor','is_admin'=>0,'hash'=>md5(time().mt_rand(10,100)),'is_active'=>'1'],
    	];

    	for ($i=0; $i<count($arr_users); $i++) {
			User::create($arr_users[$i]);
    	}

    }
}
