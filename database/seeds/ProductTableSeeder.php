<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $q = [
        	// New Arrival
        	['name'=>'Air Filter','image'=>'products/new-arrival/air-filter.jpg','image_thumb'=>'air-filter-thumb.jpg'],
        	['name'=>'Bar Pad','image'=>'products/new-arrival/bar-pad.jpg','image_thumb'=>'bar-pad-thumb.jpg'],
        	['name'=>'Brake Cluth Lever','image'=>'products/new-arrival/brake-clutch-lever.jpg','image_thumb'=>'brake-clutch-lever-thumb.jpg'],
        	['name'=>'Brake Disc Plate','image'=>'products/new-arrival/brake-disc-plate.jpg','image_thumb'=>'brake-disc-plate-thumb.jpg'],
        	['name'=>'Gloves','image'=>'products/new-arrival/gloves.jpg','image_thumb'=>'gloves-thumb.jpg'],
        	['name'=>'Gloves Half Pro-Bike','image'=>'products/new-arrival/gloves-half-probike.jpg','image_thumb'=>'gloves-half-probike-thumb.jpg'],
        	['name'=>'Head light','image'=>'products/new-arrival/headlight.jpg','image_thumb'=>'headlight-thumb.jpg'],
        	['name'=>'Helmet','image'=>'products/new-arrival/helmet.jpg','image_thumb'=>'helmet-thumb.jpg'],
        	['name'=>'Horn','image'=>'products/new-arrival/horn.jpg','image_thumb'=>'horn-thumb.jpg'],
        	['name'=>'Hub Set','image'=>'products/new-arrival/hub-set.jpg','image_thumb'=>'hub-set-thumb.jpg'],
        	['name'=>'Mono Shock','image'=>'products/new-arrival/mono-shock.jpg','image_thumb'=>'mono-shock-thumb.jpg'],
        	['name'=>'Racing Goggles','image'=>'products/new-arrival/racing-goggles.jpg','image_thumb'=>'racing-goggles-thumb.jpg'],
        	['name'=>'Rear Shock','image'=>'products/new-arrival/rear-shock.jpg','image_thumb'=>'rear-shock-thumb.jpg'],
        	['name'=>'Shifter','image'=>'products/new-arrival/shifter.jpg','image_thumb'=>'shifter-thumb.jpg'],
        	['name'=>'Swing Arm','image'=>'products/new-arrival/swing-arm.jpg','image_thumb'=>'swing-arm-thumb.jpg'],

        	// Best Seller
        	['name'=>'Bearing','image'=>'products/best-seller/bearing.resized.jpg','image_thumb'=>'best-seller/bearing.resized.jpg'],
        	['name'=>'Connecting Rod','image'=>'products/best-seller/connecting-rod.resized.jpg','image_thumb'=>'best-seller/connecting-rod.resized.jpg'],
        	['name'=>'Engine Valve','image'=>'products/best-seller/engine-valve.resized.jpg','image_thumb'=>'best-seller/engine-valve.resized.jpg'],
        	['name'=>'Fuel Filter','image'=>'products/best-seller/fuel-filter.resized.jpg','image_thumb'=>'best-seller/fuel-filter.resized.jpg'],
        	['name'=>'Gasket','image'=>'products/best-seller/gasket.resized.jpg','image_thumb'=>'best-seller/gasket.resized.jpg'],
        	['name'=>'Oil Filter','image'=>'products/best-seller/oil-filter.resized.jpg','image_thumb'=>'best-seller/oil-filter.resized.jpg'],
        	['name'=>'O-Ring','image'=>'products/best-seller/o-ring.resized.jpg','image_thumb'=>'best-seller/o-ring.resized.jpg'],
        	['name'=>'Piston Kit','image'=>'products/best-seller/Piston-Kit.resized.jpg','image_thumb'=>'best-seller/piston.resized.jpg'],
        	['name'=>'Piston Ring','image'=>'products/best-seller/Piston-Ring.resized.jpg','image_thumb'=>'best-seller/piston-ring.resized.jpg'],
        	['name'=>'Seal Valve','image'=>'products/best-seller/Seal-Valve.resized.jpg','image_thumb'=>'best-seller/seal-valve.resized.jpg'],
        	['name'=>'Spark Plug','image'=>'products/best-seller/Spark-Plug.resized.jpg','image_thumb'=>'best-seller/spark-plug.resized.jpg'],
        	['name'=>'Stator Coil','image'=>'products/best-seller/stator coil.resized.jpg','image_thumb'=>'best-seller/stator-coil.resized.jpg'],
        	
        	// MGC Products
        	['name'=>'Ball Bearing','image'=>'products/ball-bearing.resized.jpg','image_thumb'=>'mgc/ball-bearing.resized.jpg'],
        	['name'=>'Battery','image'=>'products/battery.resized.jpg','image_thumb'=>'mgc/battery.resized.jpg'],
        	['name'=>'Brake Pad','image'=>'products/brake-pad.resized.jpg','image_thumb'=>'mgc/brake-pad.resized.jpg'],
        	['name'=>'Break Shoe','image'=>'products/break-shoe.resized.jpg','image_thumb'=>'mgc/break-shoe.resized.jpg'],
        	['name'=>'Chain','image'=>'products/chain.resized.jpg','image_thumb'=>'mgc/chain.resized.jpg'],
        	['name'=>'Clutch Lining','image'=>'products/clutch-lining.resized.jpg','image_thumb'=>'mgc/clutch-lining.resized.jpg'],
        	['name'=>'Gasket','image'=>'products/gasket.resized.jpg','image_thumb'=>'mgc/gasket.resized.jpg'],
        	['name'=>'Head Light Bulb','image'=>'products/head-light-bulb.resized.jpg','image_thumb'=>'mgc/head-light-bulb.resized.jpg'],
        	['name'=>'Knuckle Bearing','image'=>'products/knuckle-bearing.resized.jpg','image_thumb'=>'mgc/knuckle-bearing.resized.jpg'],
        	['name'=>'Piston Kit','image'=>'products/piston-kit.resized.jpg','image_thumb'=>'mgc/piston-kit.resized.jpg'],
        	['name'=>'Speed Cable','image'=>'products/speed-cable.resized.jpg','image_thumb'=>'mgc/speed-cable.resized.jpg'],
        	['name'=>'Spracket','image'=>'products/spracket.resized.jpg','image_thumb'=>'mgc/spracket.resized.jpg'],
        	['name'=>'Throttle Cable','image'=>'products/throttle-cable.resized.jpg','image_thumb'=>'mgc/throttle-cable.resized.jpg'],
        	['name'=>'T Light Bulb','image'=>'products/tlight-bulb.resized.jpg','image_thumb'=>'mgc/tlight-bulb.resized.jpg'],
        	['name'=>'Tube','image'=>'products/tube.resized.jpg','image_thumb'=>'mgc/tube.resized.jpg'],
        ];

        for ($i=0; $i < count($q); $i++) { 
        	Product::create($q[$i]);
        }
    }
}
