<?php

use Illuminate\Database\Seeder;

class ProductDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// yaw nag palag if wala nako gihimuan ug loop.. gadali ko
        $q = [
        		['prod_id'=>1, 'tag_id'=>1],
        		['prod_id'=>2, 'tag_id'=>1],
        		['prod_id'=>3, 'tag_id'=>1],
        		['prod_id'=>4, 'tag_id'=>1],
        		['prod_id'=>5, 'tag_id'=>1],
        		['prod_id'=>6, 'tag_id'=>1],
        		['prod_id'=>7, 'tag_id'=>1],
        		['prod_id'=>8, 'tag_id'=>1],
        		['prod_id'=>9, 'tag_id'=>1],
        		['prod_id'=>10, 'tag_id'=>1],
        		['prod_id'=>11, 'tag_id'=>1],
        		['prod_id'=>12, 'tag_id'=>1],
        		['prod_id'=>13, 'tag_id'=>1],
        		['prod_id'=>14, 'tag_id'=>1],
        		['prod_id'=>15, 'tag_id'=>1],

				['prod_id'=>16, 'tag_id'=>2],
				['prod_id'=>17, 'tag_id'=>2],
				['prod_id'=>18, 'tag_id'=>2],
				['prod_id'=>19, 'tag_id'=>2],
				['prod_id'=>20, 'tag_id'=>2],
				['prod_id'=>21, 'tag_id'=>2],
				['prod_id'=>22, 'tag_id'=>2],
				['prod_id'=>23, 'tag_id'=>2],
				['prod_id'=>24, 'tag_id'=>2],
				['prod_id'=>25, 'tag_id'=>2],
				['prod_id'=>26, 'tag_id'=>2],
				['prod_id'=>27, 'tag_id'=>2],

				['prod_id'=>28, 'tag_id'=>3],
				['prod_id'=>29, 'tag_id'=>3],
				['prod_id'=>30, 'tag_id'=>3],
				['prod_id'=>31, 'tag_id'=>3],
				['prod_id'=>32, 'tag_id'=>3],
				['prod_id'=>33, 'tag_id'=>3],
				['prod_id'=>34, 'tag_id'=>3],
				['prod_id'=>35, 'tag_id'=>3],
				['prod_id'=>36, 'tag_id'=>3],
				['prod_id'=>37, 'tag_id'=>3],
				['prod_id'=>38, 'tag_id'=>3],
				['prod_id'=>39, 'tag_id'=>3],
				['prod_id'=>40, 'tag_id'=>3],
				['prod_id'=>41, 'tag_id'=>3],
				['prod_id'=>42, 'tag_id'=>3],
        	];

        for ($i=0; $i < count($q); $i++) { 
        	\DB::table('product_detail')->insert($q[$i]);
        }
    }
}
