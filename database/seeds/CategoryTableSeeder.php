<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $q = [
        		['category'=>'Accessories'],
        		['category'=>'Apparels'],
        		['category'=>'Parts']
        	];

        for ($i=0; $i < count($q); $i++) { 
        	Category::create($q[$i]);
        }

    }
}
