@extends('layouts.master')

@section('title', 'Kambal Pandesal | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">KAMBAL PANDESAL</h2>

			<img src="{{asset('assets/img/affiliates/kambal-pandesal-lg.png')}}" alt="kambal-pandesal" class="pull-right">

			<p>Kambal Pandesal, is a franchise business of Motormate Group of Companies in partnership with San Miguel Food Corporation. It bakes one the most nutritious and delicious bread in the country in different fillings &amp; flavors to choose like Pan de sal, Hotdog Pan de sal, Pan de sal Espana, Pan de Kape, Pan de Coco, Ensaymada, Crinkles and Whole Wheat Breads.</p>

			<p>Since most of us Filipinos cannot spend a day without eating Pan de Sal then Kambal Pan De Sal is your best choice that serves hot and freshly bake Pan De Sal a perfect match for your hot coffee any time of the day.</p>

			<p>Feel great and start your day with Kambal Pandesal.</p>

			<p>Kambal Pandesal is located in CJAM’s Quickmart, Zone 10 Zayas, Carmen Cagayan de Oro City.</p>

		</div>

	</div>

@stop