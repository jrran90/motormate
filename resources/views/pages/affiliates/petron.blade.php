@extends('layouts.master')

@section('title', 'Petron | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">PETRON</h2>

			<img src="{{asset('assets/img/affiliates/petron-lg.png')}}" alt="petron" class="pull-right" style="width:150px">

			<p>Motormate merchandise campany inc or MMCI is now franchised Petron gasoline station to provide automobile and motorcycle a good quality fuel. Petron Corporation is the largest oil refining and marketing company in the Philippines. Supplying nearly 40% of the country’s oil requirements, our world-class products and quality services fuel the lives of millions of Filipinos everyday. Our LPG brands, Gasul and Fiesta, have been an intrinsic part of our customers’ households for many decades. </p>
		</div>

	</div>

@stop