@extends('layouts.master')

@section('title', 'MGC Resort | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">STARMAC RESORT (My Golden Comfort)</h2>

			<img src="{{asset('assets/img/affiliates/mgc-resort-lg.png')}}" alt="mgc-resort" class="pull-right" width="250px">

			<p>My Golden Comfort Resort and Water Sports, one of the newest beach resort in Misamis Oriental, a place where you can relax and enjoy its facilities. MGC is an ideal place to spend quality time with your love ones, family, friends and co-employees for group vacation and team building. Furthermore, it’s a perfect venue for all kinds of party such birthday, debut, baby’s christening and a romantic place for a wedding.</p>

			<p>Experience the captivating ambiance of the resort and its sunset that creates a magnificent scene.  Make use the spacious location for your convenience and feel free to do what makes you comfortable.</p>

			<p>So what are you waiting for? Pack your bags and travel to the shores of MGC Resort. Stay overnight and experience only the best room services that goes along with our reasonable room packages.</p>

			<p>MGC, is situated away from the bustling and busy city. It’s located at Brgy. Mauswagan, Laguindingan, Mis.Or, a 45 kilometers from Cagayan de Oro and more or less one hour to travel.</p>

		</div>

	</div>

@stop