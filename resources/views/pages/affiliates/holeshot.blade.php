@extends('layouts.master')

@section('title', 'Hole Shot Gym and Fitness Center | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">HOLE SHOT GYM AND FITNESS CENTER</h2>

			<img src="{{asset('assets/img/affiliates/holeshot-lg.png')}}" alt="holeshot-gym-and-fitness-center" class="pull-right">

			<p>Living at the fittest way is one way of living the healthy way. Hole Shot Gym and Fitness Center caters the complete, the best and the magnificent program a fitness center can ever give you. With state of the art facilities and fitness instructors, surely your body will suit to our healthy society. Tired of having some heavy and watery feeling of your body? Why don’t join us at Hole Shot Gym and Fitness Center and be one of the tenacious people in our very own city.</p>


			<p>Hole Shot Gym and Fitness Center is one of the affiliate companies of Motormate Group. It operated first on March 8, 2008 at Quirino – Hayes Street, Barangay 37, Cagayan de Oro City beside Motormate Main Branch. It was established mainly for the owner and the Motormate Group of Companies’ employees to promote active and flourish style of living, but for the many and overwhelming inquiries the Fitness Center later then accepted enrollees from the public with affordable and flexible monthly fee.</p>

			<p>For years of operation, the Fitness Center has grown abundantly, not just quantitatively speaking but the area also has expanded in order to fill the comfort and contentment of our valued members. Currently, Hole Shot Gym and Fitness Center is using the high-quality and body-boosting gym equipments to give excellent experience to our Hole Shot and Fitness Center members.</p>

			<p>Hole Shot Gym and Fitness Center also participates in gym and fitness activities, in fact, last December 8, 2008, members of the fitness center joined the Mr. Jasaan Body Building Contest, surprisingly we caught Mr. Jasaan- Champion, Best Abs, Best Poser and Most Applauded. Also, last December 20, 2008, we clinched Mr. Mindanao- 1st Runner Up, Mr. Restolax, Best Poser and Best Muscularity.</p>

			<p>For more inquiries and information, please visit us Quirino – Hayes Street, Barangay 37, Cagayan de Oro City beside Motormate Group of Companies Main Branch or mail us at mail.motormategroup.com or contact us at these numbers: <br>
			<br>857-1489 / 857-1488 / 857-5799 / 72-47-31</p>

		</div>

	</div>

@stop