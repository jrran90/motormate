@extends('layouts.master')

@section('title', 'Pakals Mo Atbp. | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">PAKALS MO ATBP.</h2>

			<img src="{{asset('assets/img/affiliates/pakals-lg.png')}}" alt="pakals-mo-atbp" class="pull-right">

			<p>Finding for a place where your cravings will be satisfied? Pakals Mo Atbp is the place to be. With selections of Filipino favorites, there is no way that you will feel empty. Pakals Mo Atbp can accommodate seventy customers with different menus daily, plus the fresh veggies and desserts that await you, surely your health will be at proper state. Also, Pakals serves short orders for take out and dine in basis.</p>

			<p>Pakals accepts orders and services from private parties, family affairs, birthdays, seminars and all other gatherings at affordable prices.</p>

			<p>Through years of operation, Pakals is a must for you to visit because of its mouthwatering Filipino food including typical Filipino favorites, bulalo, fried/grilled chicken, pork delicacies and many more. The restaurant displays hospitality and evenly accommodates customers. Their native looking interior design with old-fashioned atmosphere will give you the relaxation and comfort you are looking for. The restaurant opens from Mondays through Saturdays at 7:00 am to 8:00 pm.</p>

			<p>Drop by, fill your cravings and satisfy yourselves at Motormate Building Hayes-Pinikitan Street, Cagayan de Oro City. You may Contact Pakals at these numbers: 857-3813 / 0922-840-5044.</p>

		</div>

	</div>

@stop