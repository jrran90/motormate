@extends('layouts.master')

@section('title', 'Starmac | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">STARMAC REALTY</h2>

			<img src="{{asset('assets/img/affiliates/starmac-lg.png')}}" alt="starmac" class="pull-right">			

			<p>One of the members of Motormate Group of Companies is Starmac Realty which provides fully furnished house rent, business/office space, rents on billboard and bachelor’s pad. Starmac Realty is located at MAM BLDG – Abellanosa St. Consolacion, Cagayan de Oro City.</p>

			<p>Starmac Realty provides all the ease in trying to save money when renting real estate and office space. We provide affordable properties to be rented, allowing anyone to search or select what suited to their taste or an office to stay with. Our services are one of the best realties in town.</p>

			<p>For more inquiries: <br>Call us at Tel. No. (088) 857-1488/ (088) 857-1489</p>

		</div>

	</div>

@stop