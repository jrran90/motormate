@extends('layouts.master')

@section('title', 'CDO 2 Cycles | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">CDO 2 CYCLES</h2>

			<img src="{{asset('assets/img/affiliates/2-cycles-lg.png')}}" alt="crm-digitech-prints" class="pull-right">

			<p>CDO 2 CYCLES is a one stop YAMAHA Motorcycle shop that offers all the needs of a motorcycle buyer from sales, service to spare parts. We have a complete variety of Yamaha bikes from manual to automatic motorcycles. We offer C.O.D. and Motorcycle financing services to our valued customers.  Currently, there are 6 CDO 2 CYCLES outlets located in Gusa, Tiano, Pinikitan and Puerto in Cagayan de Oro City and Malaybalay &amp; Valencia City in Bukidnon province.</p>

			<p>CDO 2 CYCLES is a member of Motormate of Group of Companies who has been the industry’s benchmark of success for 20 years and still counting. With the values, technology and thirst for success learned from its mother company, CDO 2CYCLES is determined to be the best among the rest and be the benchmark of success not only among Yamaha outlets but to the entire motorcycle industry as well.</p>

		</div>

	</div>

@stop