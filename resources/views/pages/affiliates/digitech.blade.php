@extends('layouts.master')

@section('title', 'Digitech | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">CRM DIGITECH</h2>

			<img src="{{asset('assets/img/affiliates/crm-digitech-lg.png')}}" alt="crm-digitech-prints" class="pull-right">

			<p>Established on <strong>September 8, 2009</strong></p>

			<p>CRM Digitech Prints offers variety of products and services which specifically target all our Printing needs. We highlight our tarpaulin printing, Signages, billboards, t-shirt printing, corporate and school id’s, and <strong>specializes 2 wheel and 4 wheel sticker wrap, personalized sticker printing</strong>, Sintra boards, Wall art- Murals and offers lay-outing services, actual sticker installation, offset printing services and a lot more.</p>

			<p>CRM Digitech Prints provides not just quality products and services but highlighted our most talented, competitive and well-rounded personnel.</p>

			<h4>MISSION</h4>

			<p>CRM DIGITECH PRINTS, printing solution shop that aims to provide clients with quality products and outstanding Digital &amp; OFFSET Printing services.</p>

			<p>We deliver and enhance all printing requirements of our clients through the latest technology and efficient yet excellent  results of our products and services.  We are a company that boost the artistry and make out the best of our sales and support personnel.</p>

			<p>We offer a printing solutions not just for the sake of Art but for the satisfaction index of our valued clients.</p>

			<h4>SERVICES OFFERED</h4>

			<h5 style="font-weight:bold">We Offer</h5>
			<ul>
				<li>Tarpaulin Printing</li>
				<li>Panaflex</li>
				<li>T-shirt Printing</li>
				<li>Personalized Mug</li>
				<li>PVC I.D. (Corporate &amp; School ID)</li>
				<li>Signage</li>
				<li>Billboards</li>
				<li>Flyers</li>
				<li>Calling Cards</li>
				<li>Calendars</li>
			</ul>

			<h5 style="font-weight:bold">We Specialized</h5>
			<ul>
				<li>Vehicle Sticker Wrap (for motorcycle &amp; other vehicles)</li>
				<li>Sticker Printing</li>
				<li>Wall Art/Murals</li>
				<li>Cut-out Standee/Menu Board &amp; other Advertising Materials</li>
			</ul>

		</div>

	</div>

@endsection