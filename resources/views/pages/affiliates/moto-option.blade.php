@extends('layouts.master')

@section('title', 'Moto Options | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">MOTO OPTIONS</h2>

			<img src="{{asset('assets/img/affiliates/moto-options-lg.png')}}" alt="moto-options" class="pull-right">

			<p>Motormate Group is committed to serve the people, and aims to provide motorists a high quality of motorcycles, spare parts, and accessories.</p>

			<p>As the motorcycle market in the Philippines develops, motorcycle riding has been part of the people’s lifestyle. More riders have been expressing their motorcycle lifestyle through modifications and accessorizing their bikes.</p>

			<p>To cater the needs of the growing motorcycle trend, MOTORMATE GROUP founded its exclusive motorcycle accessories, modifications, and apparels division. MOTO OPTIONS, INC. was established last April 28, 2007. MOTO OPTIONS’ commitment is to serve the people by being the aid of expressing oneself through motorcycle innovation. MOTO OPTIONS’ aim is to provide our customers a high quality of motorcycles accessories, motorist apparel and motorcycle modification through continuous research and innovations of other possibilities that we can further enhance the lifestyle and safety of both the riders and the bike.</p>

			<p>We are aiming to set a standard for an expected need. We are the only company that is completely committed and promises a total motorcycle lifestyle experience. Similar to riders who are on the go, they constantly change routes for a much exciting and enjoyable riding experience. That is our commitment; we find ways to serve the riders’ need for a more motorcycle lifestyle experience. We are MOTO OPTIONS- LIFESTYLE, PERFORMANCE, and INNOVATION.</p>

		</div>

	</div>

@stop