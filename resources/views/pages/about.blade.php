@extends('layouts.master')

@section('title', 'About Us | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">ABOUT US</h2>

			<h4>VISION</h4>

			<p><strong>MOTORMATE GROUP OF COMPANIES</strong> is committed to serve the people and aims to provide motorists with a high quality of Motorcycles, Spare parts and Accessories through excellent and reliable. We enhance the quality of life of the community we serve and are dependable partner in our nation’s progress.</p>

			<h4>MISSION</h4>

			<p><strong>MOTORMATE GROUP OF COMPANIES</strong> is renowned as the most complete and low-cost motorcycle spare parts and accessories dealer in the region. We are recognize for our un-equaled, genuine; warm and caring service to our customers. We are reputed for our commitment to excellence through through Total Customer Index Satisfaction. And we are a company benchmark for Success. We are an employer of choice providing vast professional opportunities and personal growth. Our staff moves in the same direction and cooperating with each other to achieve our corporate goals. We find enjoyment in everything we do, and we take responsibility of what we say and the decision we make and actions we take. We are an honorable company, with high sense of integrity, we do what is right and we strive to be the best we could ever be.</p>

		</div>

	</div>

@stop