@extends('layouts.master')

@section('title', 'Careers | MGC')

@section('active', 'active')

@section('main')

	<!-- <div class="row j-page-container" ng-controller="CareerController as c"> -->
	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">CAREERS</h3>

			<div class="ui-view-container">
				<div ui-view style="margin-bottom:15px"></div>
			</div>

			<footer style="font-size:12px">
				<p style="color:#fff !important">Unsolicited bulk e-mail, e-mail attachments, and junk e-mail of any kind are not accepted and will be discarded/ filtered immediately upon receipt. Please do not send/e-mail resumes concerning listed positions unless specifically told to do so in the listing. Any unsolicited Resumes will be immediately discarded.</p>
			</footer>

		</div>

	</div>

@endsection

@section('scripts')
	<script src="{{asset('app/components/careers/careerController.js')}}"></script>
	<script src="{{asset('app/components/careers/careerService.js')}}"></script>
@endsection