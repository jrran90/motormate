@extends('layouts.master')

@section('title', 'Contact Us | MGC')

@section('active', 'active')

@section('main')

	<style>
		#map {height: 350px;width:500px;border:1px solid #bbb;background:#fff;}
		#save-widget {
			width: 300px;
			box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px;
			background-color: white;
			padding: 10px;
			font-family: Roboto, Arial;
			font-size: 13px;
			margin: 15px;
		}
	</style>


	<div class="row j-page-container">
		<div class="col-md-12">
			<h2 class="page-header">CONTACT US</h2>
		</div>
	</div>


	<div class="row j-page-container">

		<div class="col-md-7">			

			<div id="map"></div>
			<!-- <div id="save-widget">
				<strong>Google Sydney</strong>
				<p>We’re located on the water in Pyrmont, with views of the Sydney Harbour Bridge, The
				Rocks and Darling Harbour. Our building is the greenest in Sydney. Inside, we have a
				"living wall" made of plants, a tire swing, a library with a nap pod and some amazing
				baristas.</p>
			</div> -->

		</div>

		<div class="col-md-5">
			
			<address>
			Email us with any questions or inquiries or you may contact us using this<br><br>Tel#: (088) 857-1488-89 / 857-5799 / 72-4731.<br>Email: hrd@motormategroup.com
			</address>

			<ul class="list-unstyled">
				<li><a href="https://www.facebook.com/MOTORMATE-388304122845/timeline/" target="_blank"><i class="fa fa-lg fa-facebook-official"></i> motormate</a></li>
			</ul>

		</div>		

	</div>

	<script>

	/*function initMap() {
		var myLatLng = new google.maps.LatLng(8.475630, 124.653664);
		var mapOptions = {
			center: myLatLng,
			zoom: 17,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		var map = new google.maps.Map(document.getElementById('map'), mapOptions);


		var widgetDiv = document.getElementById('save-widget');
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(widgetDiv);


		var marker = new google.maps.Marker({
			map: map,
			position: saveWidget.getPlace().location
		})

	}
*/



		function initMap() {
			var myLatLng = new google.maps.LatLng(8.475630, 124.653664);
			var mapOptions = {
				center: myLatLng,
				zoom: 17,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			var map = new google.maps.Map(document.getElementById('map'), mapOptions);
			
			var marker = new google.maps.Marker({
				position: myLatLng,
				title: 'Motormate Main',
			});

			// Construct a new InfoWindow
			var infoWindow = new google.maps.InfoWindow({content:'<strong>Motormate Main</strong>'});

			// Opens the InfoWindow by default
			infoWindow.open(map, marker);

			marker.setMap(map);
		}
	</script>

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6D_hkHqLL8YpFR8PhURcHfi5nnAK77KU&callback=initMap"></script>

@endsection