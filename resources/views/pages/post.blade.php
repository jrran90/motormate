@extends('layouts.master')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<h2 class="page-header">{{$post->title}}</h2>
			<h6><strong>Posted:</strong> {{ date('F d, Y',strtotime($post->created_at)) }}</h6>

			@if($post->image)
			<img src="{{ asset('assets/img/slideshows').'/'.$post->image }}" alt="{{$post->slug}}" width="800">
			@endif
		
			<p>{!! $post->content !!}</p>

		</div>

	</div>

@endsection