@extends('layouts.master')

@section('title', 'Services | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12" ng-controller="ServiceController as serv">

			<h2 class="page-header">SERVICES OFFERED<br><small style="color:#eee">We accepts all kinds of motorcycles</small></h2>

			<ul class="list-inline lst_services">
				<li ng-repeat="img in serv.images">
					<a href ng-click="serv.openLightboxModal($index)">
						<figure>
							<img ng-src="@{{img.thumbUrl}}" alt="">
							<figcaption>@{{img.name}}</figcaption>
						</figure>
					</a>
				</li>
			</ul>

		</div>

	</div>

@endsection

@section('scripts')
	<script src="{{asset('app/components/services/serviceController.js')}}"></script>
@endsection