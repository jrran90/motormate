@extends('layouts.master')

@section('title', 'News and Events | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container page-news-events">

		<div class="col-md-12">

			<h2 class="page-header">NEWS &amp; EVENTS</h2>

			@foreach ($posts as $post)

			<div class="media">

				@if($post->image)
				<div class="media-left">
					<a href="{{route('show-post',$post->slug)}}">
						<img class="media-object" src="{{asset('assets/img/slideshows/thumb').'/'.$post->image}}" width="100" style="height:80px;object-fit:cover">
					</a>
				</div>
				@endif
				<div class="media-body">
					<h4 class="media-heading"><a href="{{route('show-post',$post->slug)}}">{{$post->title}}</a></h4>
					<h6>{{date('F d, Y', strtotime($post->created_at))}}</h6>
					<p>{!! strip_tags(str_limit($post->content, 200)) !!}</p>
				</div>
			</div>

			@endforeach

		</div>

	</div>

@stop