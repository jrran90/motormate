@extends('layouts.master')

@section('title', 'Products | MGC')

@section('active', 'active')

@section('main')

	<div class="row j-page-container">

		<div class="col-md-12">

			<div ui-view></div>

		</div>

	</div>

@endsection

@section('scripts')
	<script src="{{asset('app/components/products/productController.js')}}"></script>
	<script src="{{asset('app/components/products/productService.js')}}"></script>
@endsection