<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>

    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/login.css') }}">

</head>
<body>

<div class="container">

    <div class="row">

        <div id="wrap_login" class="col-md-4 col-md-offset-4">

            {!! Session::get('msg') !!}

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input. Sus!<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif            

            <header>MOTORMATE</header>

            <form method="POST" action="{{url('/login')}}" novalidate>
                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="InputEmail">E-mail</label>
                    <input type="email" class="form-control" id="InputEmail" placeholder="Email" name="email" value="{{ old('email') }}" autofocus autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="InputPassword">Password</label>
                    <input type="password" class="form-control" id="InputPassword" placeholder="Password" name="password">
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox"> Remember Me
                    </label>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                </div>
            </form>
        </div>

    </div>

</div>
    
</body>
</html>


