<!DOCTYPE html>
<html lang="en" ng-app="mgc">
<head>

    <meta charset="UTF-8">
    <title>Dashboard</title>

    <!-- le stylesheet -->
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('bower_components/components-font-awesome/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('bower_components/angular-loading-bar/build/loading-bar.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/dashboard.css')}}">

</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" ui-sref="dashboard">MOTORMATE Dashboard</a>
			</div>

			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome {{Auth::user()->display_name}} <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Profile</a></li>
							<li><a href="#">Settings</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="{{url('auth/logout')}}">Logout</a></li>
						</ul>
					</li>
				</ul>

			</div>
		</div>
	</nav>



	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<li ui-sref-active="active"><a ui-sref="dashboard">Dashboard <span class="sr-only">(current)</span></a></li>
					<li ui-sref-active="active"><a ui-sref="post.list">Post</a></li>
				</ul>

				<h4>Master Files</h4>
				<ul class="nav nav-sidebar">
					@if (Auth::user()->is_admin)<li ui-sref-active="active"><a ui-sref="users">Users</a></li>@endif
					<li ui-sref-active="active"><a ui-sref="products">Products</a></li>
					<li ui-sref-active="active"><a ui-sref="services">Services</a></li>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

				<div ui-view></div>

				<!-- <h1 class="page-header">Dashboard</h1> -->

				<!-- <div class="row placeholders">
				<div class="col-xs-6 col-sm-3 placeholder">
				<img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">
				<h4>Label</h4>
				<span class="text-muted">Something else</span>
				</div>
				<div class="col-xs-6 col-sm-3 placeholder">
				<img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">
				<h4>Label</h4>
				<span class="text-muted">Something else</span>
				</div>
				<div class="col-xs-6 col-sm-3 placeholder">
				<img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">
				<h4>Label</h4>
				<span class="text-muted">Something else</span>
				</div>
				<div class="col-xs-6 col-sm-3 placeholder">
				<img data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="Generic placeholder thumbnail">
				<h4>Label</h4>
				<span class="text-muted">Something else</span>
				</div>
				</div>

				<h2 class="sub-header">Section title</h2>
				<div class="table-responsive">

				</div> -->
			</div>
		</div>
	</div>


<!-- kindly remove this one -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- le javascript -->
<script src="{{asset('bower_components/angular/angular.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-animate/angular-animate.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-ui-router/release/angular-ui-router.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-sanitize/angular-sanitize.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-loading-bar/build/loading-bar.min.js')}}"></script>

	<!-- tiny MCE -->
    <script type="text/javascript" src="{{asset('bower_components/tinymce-dist/tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/angular-ui-tinymce/src/tinymce.js')}}"></script>

    <!-- ng-file-upload -->
    <script src="{{asset('bower_components/ng-file-upload/ng-file-upload.min.js')}}"></script>

  	<script src="{{asset('app/app_dashboard.js')}}"></script>
  		<script src="{{asset('app/components/_dashboard/controller.js')}}"></script>
  		<script src="{{asset('app/components/_dashboard/service.js')}}"></script>

</body>
</html>