<!DOCTYPE html>
<html lang="en" ng-app="mgc">
<head>

    <meta charset="UTF-8">
    <title>@yield('title','Motormate Group of Companies')</title>

    <!-- le stylesheet -->
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('bower_components/components-font-awesome/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('bower_components/angular-loading-bar/build/loading-bar.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('bower_components/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.css')}}">


</head>
<body>

    <header id="header">

        <div class="container">

            <div class="row">

                <div class="brand_container">
                    <a href="{{url('/')}}" class="pull-left">
                        <img src="{{asset('assets/img/motormate.png')}}" alt="" class="brand_logo">
                    </a>

                    <h1 class="pull-left brand_name">
                        <span>motormate<br><small>group of companies</small></span>
                    </h1>
                </div>

                <nav class="lst_affiliates text-center">

                    <ul class="list-inline">
                        <li>
                            <a href="{{url('affiliates/cdo-2-cycles')}}" tooltip="CDO 2 Cycles" tooltip-placement="bottom">
                                <img src="{{asset('assets/img/affiliates/2-cycles.png')}}" alt="2-cycles" width="70">
                            </a>
                        </li>
                        <li>
                            <a href="{{url('affiliates/holeshot-gym-and-fitness-center')}}" tooltip="Hole Shot Gym and Fitness Center" tooltip-placement="bottom">
                                <img src="{{asset('assets/img/affiliates/holeshot.png')}}" alt="holeshot-fitness &amp; gym" width="80">
                            </a>
                        </li>
                        <li>
                            <a href="{{url('affiliates/moto-options')}}" tooltip="Moto Options" tooltip-placement="bottom">
                                <img src="{{asset('assets/img/affiliates/moto-options.png')}}" alt="moto-options" width="80">
                            </a>
                        </li>
                        <li>
                            <a href="{{url('affiliates/pakals-mo-at-iba-pa')}}" tooltip="Pakals Mo Atbp." tooltip-placement="bottom">
                                <img src="{{asset('assets/img/affiliates/pakals.png')}}" alt="pakals-mo-atbp." width="80">
                            </a>
                        </li>
                        <li>
                            <a href="{{url('affiliates/starmac')}}" tooltip="Starmac Realty" tooltip-placement="bottom">
                                <img src="{{asset('assets/img/affiliates/starmac.png')}}" alt="starmac" width="80">
                            </a>
                        </li>
                        <li>
                            <a href="{{url('affiliates/crm-digitech')}}" tooltip="CRM Digitech" tooltip-placement="bottom">
                                <img src="{{asset('assets/img/affiliates/crm-digitech.png')}}" alt="crm-digitech">
                            </a>
                        </li>
                        <li>
                            <a href="{{url('affiliates/mgc-resort')}}" tooltip="MGC Resort" tooltip-placement="bottom">
                                <img src="{{asset('assets/img/affiliates/mgc-resort.png')}}" alt="mgc-resort" width="60">
                            </a>
                        </li>
                        <li>
                            <a href="{{url('affiliates/petron')}}" tooltip="Petron" tooltip-placement="bottom">
                                <img src="{{asset('assets/img/affiliates/petron.png')}}" alt="petron" width="35">
                            </a>
                        </li>
                        <li>
                            <a href="{{url('affiliates/kambal-pandesal')}}" tooltip="Kambal Pandesal" tooltip-placement="bottom">
                                <img src="{{asset('assets/img/affiliates/kambal-pandesal.png')}}" alt="kambal-pandesal" width="100">
                            </a>
                        </li>
                    </ul>

                </nav>

                <nav id="main_menu" class="j_menu">

                    <ul class="pull-left">
                        <li><a href="{{url('/')}}" class="menu_home"><i class="fa fa-home fa-lg"></i></a></li>
                        <li><a href="{{url('/services')}}" class="@if(Request::url() === url('/services')) @yield('active') @endif">services</a></li>
                        <li><a href="{{url('/products#/category/new-arrival')}}" class="@if(Request::url() === url('/products')) @yield('active') @endif">products</a></li>
                        <li><a href="{{url('/news-and-events')}}" class="@if(Request::url() === url('/news-and-events')) @yield('active') @endif">News &amp; Events</a></li>
                        <li><a href="{{url('/careers#/list')}}" class="@if(Request::url() === url('/careers')) @yield('active') @endif">careers</a></li>
                        <li><a href="{{url('/about-us')}}" class="@if(Request::url() === url('/about-us')) @yield('active') @endif">about us</a></li>
                        <li><a href="{{url('/contact-us')}}" class="@if(Request::url() === url('/contact-us')) @yield('active') @endif">contact us</a></li>
                    </ul>

                    <ul class="pull-right social-media">
                        <li><a href="https://www.facebook.com/MOTORMATE-388304122845/timeline/"><i class="fa fa-lg fa-facebook-official"></i></a></li>
                    </ul>

                </nav>

            </div>

        </div>  

    </header>


    <main id="main">

        <div class="container">

            @yield('main')

        </div>

    </main>


    <footer id="footer">

        <div class="container">

            &copy; Copyright {{ date('Y') }} | Motormate Group of Companies  

            <ul class="pull-right list-inline">
                <li><a href="https://www.facebook.com/MOTORMATE-388304122845/timeline/"><i class="fa fa-facebook-official fa-lg"></i></a></li>
            </ul>

        </div>

    </footer>


<!-- le javascript -->
<script src="{{asset('bower_components/angular/angular.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-animate/angular-animate.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-ui-router/release/angular-ui-router.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-sanitize/angular-sanitize.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-loading-bar/build/loading-bar.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.js')}}"></script>

<script src="{{asset('app/app.js')}}"></script>
    @yield('scripts')

</body>
</html>