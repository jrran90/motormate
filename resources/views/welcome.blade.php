@extends('layouts.master')

@section('scripts')
    <script src="{{asset('app/components/home/homeController.js')}}"></script>
    <script src="{{asset('app/components/home/homeService.js')}}"></script>
@endsection

@section('main')

{{-- Slider Section --}}
<div class="row">

    <div id="slider_main" ng-controller="SliderController as slider">

        <carousel interval="slider.myInterval" no-wrap="slider.noWrapSlides">
            <slide ng-repeat="slide in slider.slides" active="slide.active">
                <img ng-src="assets/img/slideshows/@{{slide.image}}" style="margin:auto;">
            </slide>
        </carousel>

    </div>

</div>


{{-- Category Section --}}
<div class="row">

    <div class="col-md-12 text-center" id="menu_category_prod" ng-controller="ProductHomeController as product">
        <nav class="j_menu_cat">
            <ul class="nav nav-pills">
                <li ng-class="{active:product.isSelected(1)}"><a href ng-click="product.selectTab(1)" class="btn-lg">New Arrival</a></li>
                <li ng-class="{active:product.isSelected(2)}"><a href ng-click="product.selectTab(2)" class="btn-lg">Best Seller</a></li>
                <li ng-class="{active:product.isSelected(3)}"><a href ng-click="product.selectTab(3)" class="btn-lg">MGC Products</a></li>
            </ul>
        </nav>

        <div class="j_menu_cat_content">

            <ul ng-if="product.isSelected(1)">
                <li ng-repeat="image in product.imgNA">
                    <a href ng-click="product.openLightboxModal($index,'na')" style="outline:0">
                        <figure>
                            <img ng-src="@{{image.thumbUrl}}" alt="">
                            <figcaption>@{{image.name}}</figcaption>
                        </figure>
                    </a>
                </li>
            </ul>
            <ul ng-if="product.isSelected(2)">
                <li ng-repeat="image in product.imgBS">
                    <a href ng-click="product.openLightboxModal($index,'bs')" style="outline:0">
                        <figure>
                            <img ng-src="@{{image.thumbUrl}}" alt="">
                            <figcaption>@{{image.name}}</figcaption>
                        </figure>
                    </a>
                </li>
            </ul>
            <ul ng-if="product.isSelected(3)">
                <li ng-repeat="image in product.imgMP">
                    <a href ng-click="product.openLightboxModal($index,'mp')" style="outline:0">
                        <figure>
                            <img ng-src="@{{image.thumbUrl}}" alt="">
                            <figcaption>@{{image.name}}</figcaption>
                        </figure>
                    </a>
                </li>
            </ul>

            <div>
                <a href="/products#/category/@{{product.viewAllUrl}}" class="btn btn-primary btn-view-all">View All @{{product.viewAllCaption}} &rarr;</a>
            </div>

        </div>
    </div>

</div>


{{-- Events Section --}}
<div class="row" id="lst_events">

    <!-- <i class="fa fa-calendar-check-o" style="position:absolute;left:30%;top:28%;font-size:130px;color:#aaa;transform:rotate(-5deg)"></i>  -->

    <h2 class="text-center">News &amp; Events<small><a href="{{url('news-and-events')}}">View All</a></small></h2>

    @forelse($posts as $post)

        <div class="col-md-6 post-wrap-content">

            @if ($post->image)
            <div class="pull-left thumbnail" style="margin-right: 10px; margin-bottom: 0px;">
                <a href="{{ route('show-post',$post->slug) }}">
                    <img src="{{ asset('assets/img/slideshows/thumb').'/'.$post->image }}" alt="{{$post->slug}}" style="width: 80px; height: 80px;object-fit: cover;">
                </a>
            </div>
            @endif

            <div class="post-content">

                <h3><a href="{{ route('show-post',$post->slug) }}">{{$post['title']}}</a></h3>

                <p>{!! strip_tags(str_limit($post->content, 250)) !!}<a href="{{route('show-post',$post->slug)}}">Read more</a></p>

            </div>

        </div>

    @empty

        <div class="col-md-12 text-center">
            <em class="text-center">No events</em>
        </div>

    @endforelse

</div>

@endsection