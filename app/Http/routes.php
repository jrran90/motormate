<?php

Route::get('/', 				'PageController@getIndex');

Route::get('/services',			'PageController@getServices');
Route::get('/products',			'PageController@getProducts');

// news and events
Route::get('/news-and-events',	'PageController@getNewsAndEvents');
// slugs here

Route::get('/careers', 			'PageController@getCareers');
Route::get('/about-us', 		'PageController@getAbout');
Route::get('/contact-us', 		'PageController@getContact');


/**
 * Affiliates
 * Note: this one can be simplified
 */
Route::group(['prefix'=>'affiliates'], function () {

	// create a template for affilates
	Route::get('cdo-2-cycles',						'PageController@getAff2Cycles');
	Route::get('holeshot-gym-and-fitness-center',	'PageController@getAffHoleshot');
	Route::get('moto-options',						'PageController@getAffMotoOptions');
	Route::get('pakals-mo-at-iba-pa',				'PageController@getAffPakals');
	Route::get('starmac',							'PageController@getAffStarmac');
	Route::get('crm-digitech',						'PageController@getAffDigitech');
	Route::get('mgc-resort',						'PageController@getAffMGCResort');
	Route::get('petron',							'PageController@getAffPetron');
	Route::get('kambal-pandesal',					'PageController@getAffKambalPandesal');

});

/**
 * Blog
 */
Route::get('news-and-events/{slug}',	['as'=>'show-post','uses'=>'PostController@showPost']);


// API
Route::group(['prefix'=>'api'], function () {

	Route::resource('product',			'ProductController',['except'=>['create','edit']]);
	Route::get('product/category/{id}',	'ProductController@showCategory');
	Route::resource('career',			'CareerController',['only'=>['index','show']]);

	// Dashboard
	Route::resource('user',	'UserController',['except'=>['create','edit']]);

	// Post
	Route::resource('post',		'PostController',['except'=>['create','edit','update']]);
	Route::post('post/{id}', 	'PostController@updatePost');

});


Route::get('dashboard',		'HomeController@index');


Route::get('login',			'AuthController@getLogin');
Route::post('login',		'AuthController@postLogin');
Route::get('auth/logout',	'AuthController@getLogout');