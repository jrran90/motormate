<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // $img = [
        //  ["url"=>"assets/img/products/6.jpg", "thumbUrl"=>"assets/img/products/6.jpg", "cat_id"=>"1"],
        //  ["url"=>"assets/img/products/7.jpg", "thumbUrl"=>"assets/img/products/7.jpg", "cat_id"=>"2"],
        //  ["url"=>"assets/img/products/8.jpg", "thumbUrl"=>"assets/img/products/8.jpg", "cat_id"=>"1"],
        //  ["url"=>"assets/img/products/9.jpg", "thumbUrl"=>"assets/img/products/9.jpg", "cat_id"=>"1"],
        //  ["url"=>"assets/img/products/10.jpg", "thumbUrl"=>"assets/img/products/10.jpg", "cat_id"=>"1"],
        //  ["url"=>"assets/img/products/11.jpg", "thumbUrl"=>"assets/img/products/11.jpg", "cat_id"=>"2"],
        //  ["url"=>"assets/img/products/22.jpg", "thumbUrl"=>"assets/img/products/22.jpg", "cat_id"=>"1"],
        //  ["url"=>"assets/img/products/23.jpg", "thumbUrl"=>"assets/img/products/23.jpg", "cat_id"=>"1"],
        //  ["url"=>"assets/img/products/24.jpg", "thumbUrl"=>"assets/img/products/24.jpg", "cat_id"=>"2"],
        // ];

        // return $img;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Display the specified resource based on category.
     *
     * @param  int  $tag ['new-arrival','best-seller','mgc-product']
     * @return Response
     */
    public function showCategory($tag)
    {
        $q = DB::table('product AS p')
                    ->join('product_detail AS dtl', 'p.id','=','dtl.prod_id')
                    ->join('tag AS t', 'dtl.tag_id','=','t.id')
                    ->where('t.tag',$tag)
                        ->get();

        return $q;
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
