<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Career;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // should only show date range date_created>={{date}}<=date_end'
        // employment type [part-time,full-time,contract]
        // add timestamps
        return DB::table('career')
                    // ->select('job_title')
                    ->whereRaw('ended_at>=CURDATE()')
                        ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Career::whereRaw('ended_at>=CURDATE()')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
