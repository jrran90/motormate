<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except'=>'getLogout']);
    }

    public function getLogin()
    {
    	return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email'=>'required|email','password'=>'required'
        ]);

        $credentials = ['email'=>$request->get('email'),'password'=>$request->get('password'),'is_active'=>1];

    	if (Auth::attempt($credentials)) {
    		return redirect()->intended('dashboard');
    	}

        return redirect('login')->withInput()
                    ->with('msg','<div class="alert alert-danger text-center">Incorrect Username/Password!</div>');
    }

    public function getLogout()
    {
        Auth::logout();

        return redirect('/login');
    }
}
