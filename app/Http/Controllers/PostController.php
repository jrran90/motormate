<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Post;
use Auth;
use Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Post::orderBy('created_at','desc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $data = [
                    'user_id'       => Auth::id(),
                    'title'         => $request->get('title'),
                    'content'       => $request->get('content'),
                ];

        if ($request->hasFile('file')) {

            $destinationPath = public_path().'/assets/img/slideshows/';

            // generate new filename but retaining its original extension
            $img = str_random(15).rand(10,99).'.'.$request->file('file')->getClientOriginalExtension();

            // store image
            $data['image'] = $img;

            // upload img
            $request->file('file')->move($destinationPath, $img);   // moved original image

            $upload = Image::make($destinationPath.'/'.$img)
                           ->resize(960,450)->save($destinationPath.'/'.$img)
                           ->resize(null,80, function ($constraint) {
                                $constraint->aspectRatio();
                           })->save($destinationPath.'/thumb/'.$img);

        }

        // save data
        Post::create($data);

        return response()->json(['msg'=>'Successfully Added!']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Post::find($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return Post::destroy($id);
    }


    /**
     * Show specific post
     *
     * @param  string $slug
     * @return Response
     */
    public function showPost($slug)
    {
        $post = Post::where('slug',$slug)->first();

        return view('pages.post', compact('post'));
    }

    /**
     * Update specific post
     *
     * Since the app uses ng-file-upload plugin and method PUT isn't functioning accordingly [server-issues],
     * so as part of the remedy, I'll opt for using method POST for the meantime
     *
     * @param  Request  $request
     * @param  int $id
     * @return Response
     */
    public function updatePost(Request $request, $id)
    {
        $q = Post::find($request->get('post_id'));

        $q->title   = $request->get('title');
        $q->content = $request->get('content');

        $img = $request->get('image');  // default image

        if ($request->hasFile('file')) {

            $destinationPath  = public_path().'/assets/img/slideshows/';    // destination path

            $file = $request->file('file');

            $ext = $file->getClientOriginalExtension();                     // get extension

            $img = str_random(15).rand(10,99).'.'.$ext;                     // new filename            

            // upload img
            $file->move($destinationPath, $img);   // move original image

            $upload = Image::make($destinationPath.'/'.$img)
                           ->resize(960,450)->save($destinationPath.'/'.$img)
                           ->resize(null,80, function ($constraint) {
                                $constraint->aspectRatio();
                           })->save($destinationPath.'/thumb/'.$img);

            $q->image = $img;

            if (@getimagesize($destinationPath.$request->get('image')) && @getimagesize($destinationPath.'/thumb/'.$request->get('image'))) {
                unlink($destinationPath.$request->get('image'));                
                unlink($destinationPath.'/thumb/'.$request->get('image'));
            }


        }

        $q->save();

        return response()->json(['msg'=>'Successfully Updated!','new_img'=>$img]);

    }

}
