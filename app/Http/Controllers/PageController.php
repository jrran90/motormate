<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;

class PageController extends Controller
{

	public function getIndex()
	{
		$posts = Post::orderBy('created_at','desc')->take(2)->get();

		return view('welcome', compact('posts'));
	}

	public function getServices()
	{
		return view('pages.services');
	}

	public function getProducts()
	{
		return view('pages.products');
	}

	public function getNewsAndEvents()
	{

		$posts = Post::orderBy('created_at','desc')->get();

		return view('pages.news-and-events', compact('posts'));

	}	

	public function getCareers()
	{
		return view('pages.careers');
	}

	public function getAbout()
	{
		return view('pages.about');
	}

	public function getContact()
	{
		return view('pages.contact');
	}


	/**
	 * List of Affiliates
	 *
	 */
	public function getAff2Cycles()
	{
		return view('pages.affiliates.2-cycles');
	}
	public function getAffHoleshot()
	{
		return view('pages.affiliates.holeshot');
	}
	public function getAffMotoOptions()
	{
		return view('pages.affiliates.moto-option');
	}
	public function getAffPakals()
	{
		return view('pages.affiliates.pakals');
	}
	public function getAffStarmac()
	{
		return view('pages.affiliates.starmac');
	}
	public function getAffDigitech()
	{
		return view('pages.affiliates.digitech');
	}
	public function getAffMGCResort()
	{
		return view('pages.affiliates.mgc-resort');
	}
	public function getAffPetron()
	{
		return view('pages.affiliates.petron');
	}
	public function getAffKambalPandesal()
	{
		return view('pages.affiliates.kambal-pandesal');
	}

}
