<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
	use SoftDeletes;

    protected $table = 'post';

    protected $guarded = [];

    public function setTitleAttribute($value)
    {

    	/*$this->attributes['title'] = $value;

    	if (!$this->exists) {
    		$this->attributes['slug'] = str_slug($value);
    	}*/

        $this->attributes['title'] = $value;

        $slug = str_slug($value);

        $slugCnt = $this->whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->count();

        $this->attributes['slug'] = ($slugCnt>0)?"{$slug}-{$slugCnt}" : $slug;

    }
}
