angular
	.module('mgc', [
			'ui.bootstrap',
			'ngAnimate',
			'ui.router',
			'ngSanitize',
			'angular-loading-bar',
			'ui.tinymce',
			'ngFileUpload',
		])

	.directive('onErrorSrc', function () {
		return {
			link: function (scope, element, attrs) {
				element.bind('error', function () {
					if (attrs.src != attrs.onErrorSrc) {
						attrs.$set('src', attrs.onErrorSrc);
					}
				});
			}
		};
	})

	.config(['$stateProvider','$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

		$urlRouterProvider.otherwise("/");

		$stateProvider

			.state('dashboard', {
				url: '/',
				templateUrl:'app/components/_dashboard/partials/dashboardView.html',
				// controller: 'UserController as user',
			})

			.state('post', {
				url: '/post',
				templateUrl:'app/components/_dashboard/partials/post/index.html',
				controller: 'PostController as p',
			})
				.state('post.list', {
					url: '/list',
					templateUrl:'app/components/_dashboard/partials/post/post.list.html',
					controller: function ($scope) {
						// console.log('awafsad')
					}
				})			
				.state('post.add', {
					url: '/add',
					templateUrl:'app/components/_dashboard/partials/post/post.add.html',
					controller: function ($scope) {
						// console.log('awafsad')
					}
				})
				.state('post.edit', {
					url: '/:id/edit',
					templateUrl:'app/components/_dashboard/partials/post/post.edit.html',
					controller: 'PostController as p'
				})

			.state('users', {
				url: '/users',
				templateUrl:'app/components/_dashboard/partials/userView.html',
				controller: 'UserController as u',
			})

			.state('products', {
				url: '/products',
				templateUrl:'app/components/_dashboard/partials/productView.html',
				// controller: 'ProductController as prod',
			})

			.state('services', {
				url: '/services',
				templateUrl:'app/components/_dashboard/partials/serviceView.html',
				// controller: 'CareerController as c',
			})
				// .state('careersDetail', {
				// 	url: '/:career_id',
				// 	templateUrl: 'app/components/careers/careerDetailView.html',
				// 	controller: 'CareerController as c',
				// })

	}]);