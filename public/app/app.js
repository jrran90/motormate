angular
	.module('mgc', [
			'ui.bootstrap',
			'ngAnimate',
			'bootstrapLightbox',
			'ui.router',
			'ngSanitize',
			'angular-loading-bar',
		])

	.config(['$stateProvider','$urlRouterProvider','LightboxProvider', function ($stateProvider, $urlRouterProvider, LightboxProvider) {

		// Set custom template
		LightboxProvider.templateUrl = '/app/shared/lightbox-tpl.html'

		LightboxProvider.getImageUrl = function (image) {
			return '/assets/img/'+ image.image;
		};

		LightboxProvider.getImageCaption = function (image) {
			return image.name;
		};

		// $urlRouterProvider.otherwise("/")

		$stateProvider

			.state('products', {
				url: '/category/:category',
				templateUrl:'app/components/products/productView.html',
				controller: 'ProductController as prod',
			})

			.state('careers', {
				url: '/list',
				templateUrl:'app/components/careers/careerView.html',
				controller: 'CareerController as c',
			})
				.state('careersDetail', {
					url: '/:career_id',
					templateUrl: 'app/components/careers/careerDetailView.html',
					controller: 'CareerController as c',
				})

	}]);