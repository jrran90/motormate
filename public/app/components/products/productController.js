(function() {
	'use strict';

	angular
		.module('mgc')
		.config(config)
		.controller('ProductController', ProductController);

	function config(LightboxProvider) {
		LightboxProvider.templateUrl = '/app/components/products/productLightboxModal.html';

		LightboxProvider.getImageUrl = function (image) {
			return '/assets/img/'+ image.image;
		};

		LightboxProvider.getImageCaption = function (image) {
			return image.name;
		};
	}


	function ProductController(Lightbox, Product, $stateParams) {

		var vm = this;

		Product.showCat($stateParams.category)
			.then(function (res) {
				vm.images = res.data;
			});

		vm.openLightboxModal = function (index) {
			Lightbox.openModal(vm.images, index);
		};

	}

})();