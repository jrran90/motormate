(function() {
	'use strict';

	angular
		.module('mgc')
		.factory('Product', Product);

	function Product($http) {

		return {
			get: function () {
				return $http.get('api/product');
			},
			showCat: function (cat_slug) {
				return $http.get('api/product/category/'+cat_slug);
			}
		}

	}

})();