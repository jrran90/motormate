(function () {
	'use strict';

	angular
		.module('mgc')
		.controller('UserController', UserController)
		.controller('PostController', PostController);

	// error callback
	function errorCallback(e) {
		alert('Whoops, looks like something went wrong with the server.\n\nPanawag tabang kaw !!');
		console.log(e.data)
	}



	function UserController(User) {
		var vm = this;

		User.get()
			.then(function (res) {
				if (res.status==200) {
					vm.users = res.data;
				}
			}, errorCallback);
	}

	function PostController(Post, Upload, $stateParams) {
		var vm = this;

		vm.tinymceOptions = {
			onChange: function(e) {
				// put logic here for keypress and cut/paste changes
			},
			inline: false,
			skin: 'lightgray',
			theme : 'modern',
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor colorpicker textpattern imagetools"
		    ],
		    toolbar1: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview media | forecolor backcolor",
		    image_advtab: true,
		    format: 'html'	// fixes issues regarding when editor is empty, i.e. creating this tag data-mce-bogus....
		};


		disp_post();
		vm.sbPost 		= sbPost;		// add post
		vm.editPost 	= editPost;
		vm.rmPost 		= rmPost;		// remove post | put to trash






		function disp_post() {
			Post.get()
				.then(function (res) {
					vm.posts = res.data;
				});
		}
		Post.show($stateParams.id)
			.then(function (res) {
				vm.updatePost = {
					title: 		res.data.title,
					content: 	res.data.content,
					image: 		res.data.image,
				};
			});


		function sbPost() {
			Post.store(vm.post)
				.then(function (res) {
					alert(res.data.msg);
					vm.post = "";
					document.getElementById("inputPostTitle").focus();
					disp_post(); // minimize this one
					// console.log('Success ' + res.config.data.file.name + 'uploaded. Response: ' + res.data);
				},errorCallback
				, function (evt) {
		            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
	        	})			
		}
		function editPost() {
			Post.update(angular.extend(vm.updatePost,{post_id: $stateParams.id}))
				.then(function (res) {

					alert(res.data.msg);

					vm.updatePost.image = res.data.new_img;
					vm.updatePost.file = ""

				},errorCallback);
		}
		function rmPost(index,id) {
			if (confirm("You sure you want to remove this post?")) {
				Post.destroy(id)
					.then(function (res) {
						alert("Post Removed!");
						vm.posts.splice(index,1);
						disp_post();
					},errorCallback);
			}
		}

	}


})();