(function () {
	'use strict';

	angular
		.module('mgc')
		.factory('User', User)
		.factory('Post', Post);


	function User($http) {
		return {
			get: function () {
				return $http.get('api/user');
			},
			// show: function (id) {
			// 	return $http.get('api/career/'+id);
			// }
		};
	}

	function Post($http, Upload) {
		return {
			get: function () {
				return $http.get('api/post');
			},
			show: function (id) {
				return $http.get('api/post/'+id);
			},
			store: function (data) {
				// return $http({
				// 	method:  'POST',
				// 	url: 	 'api/post',
				// 	headers: {'Content-Type':'application/x-www-form-urlencoded'},
				// 	data: 	 angular.element.param(data)
				// });
				return Upload.upload({
					url: 'api/post',
					data: data
				});
			},
			update: function (data) {
				return Upload.upload({
					url: 'api/post/'+data.post_id,
					data: data,
					// method: 'PUT' -- Note: method PUT not functioning
				});
			},
			destroy: function (id) {
				return $http.delete('api/post/'+id);
			}
		}
	}


})();