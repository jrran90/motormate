(function() {
	'use strict';

	angular
		.module('mgc')
		.factory('Career', Career);


	function Career($http) {

		return {
			get: function () {
				return $http.get('api/career');
			},
			show: function (id) {
				return $http.get('api/career/'+id);
			}
		};

	}

})();