(function() {
	'use strict';

	angular
		.module('mgc')
		.controller('CareerController', CareerController);


	function CareerController(Career,$stateParams) {

		var vm = this;

		Career.get()
			.then(function (res) {
				vm.careers = res.data;
			});

		Career.show($stateParams.career_id)
			.then(function (res) {
				vm.careerTitle = res.data.job_title;
				vm.careerDtl = res.data.details;
			});
	}

})();