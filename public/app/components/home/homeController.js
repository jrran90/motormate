(function() {
	'use strict';

	angular
		.module('mgc')
		.config(config)
		.controller('SliderController',			SliderController)
		.controller('ProductHomeController',	ProductHomeController)
		.controller('NewsEventsController',		NewsEventsController);


	function config(LightboxProvider) {
		LightboxProvider.templateUrl = '/app/components/products/productLightboxModal.html';

		LightboxProvider.getImageUrl = function (image) {
			return '/assets/img/'+ image.image;
		};

		LightboxProvider.getImageCaption = function (image) {
			return image.name;
		};

	}


	function SliderController(Post) {

		var vm = this;

		vm.myInterval     = 3000;
		vm.noWrapSlides   = false; // will loop the slides

		/*Post.get()
			.then(function (res) {
				vm.slides = res.data;
				console.log(vm.slides);
			});*/

		// solve this, perhaps use another slider
		vm.slides = [
			{image:'promo1.png',content: '<b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Qui assumenda amet dolorem, recusandae eveniet quisquam illo ipsa cumque numquam quia, doloribus facere sit natus, possimus! Dolorum voluptates consequatur inventore animi.'},
			{image:'promo-2.png',content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui assumenda amet dolorem, recusandae eveniet quisquam illo ipsa cumque numquam quia, doloribus facere sit natus, possimus! Dolorum voluptates consequatur inventore animi.'},
			{image:'promo.jpg',content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui assumenda amet dolorem, recusandae eveniet quisquam illo ipsa cumque numquam quia, doloribus facere sit natus, possimus! Dolorum voluptates consequatur inventore animi.'},
		];

		// console.log(vm.slides)

	}


	function ProductHomeController(Lightbox) {

		var vm = this;

		// default values
		vm.tab = 1;
		vm.viewAllUrl 		= 'new-arrival';
		vm.viewAllCaption	= 'New Arrival';

		vm.selectTab = function(setTab) {

			vm.tab = setTab;
			
			if (setTab == 2) {
				vm.viewAllUrl = 'best-seller';
				vm.viewAllCaption	= 'Best Seller';
			} else if (setTab == 3) {
				vm.viewAllUrl = 'mgc-product';
				vm.viewAllCaption	= 'MGC Product';
			} else {
				vm.viewAllUrl = 'new-arrival';
				vm.viewAllCaption	= 'New Arrival';
			}

		}

		vm.isSelected = function(checkTab) {
			return vm.tab === checkTab;
		}

		// limit to 5 images only -- query backend
		vm.imgNA = [
			{image:'products/new-arrival/air-filter.jpg', thumbUrl:'assets/img/products/thumbnails/air-filter-thumb.jpg', name:'Air Filter'},
			{image:'products/new-arrival/bar-pad.jpg', thumbUrl:'assets/img/products/thumbnails/bar-pad-thumb.jpg',name:'Bar Pad'},
			{image:'products/new-arrival/brake-clutch-lever.jpg', thumbUrl:'assets/img/products/thumbnails/brake-clutch-lever-thumb.jpg',name:'Brake Cluth Lever'},
			{image:'products/new-arrival/brake-disc-plate.jpg', thumbUrl:'assets/img/products/thumbnails/brake-disc-plate-thumb.jpg',name:'Brake Disc Plate'},
			{image:'products/new-arrival/helmet.jpg', thumbUrl:'assets/img/products/thumbnails/helmet-thumb.jpg',name:'Helmet'},
		];
		// Best Seller
		vm.imgBS = [
			{image:'products/best-seller/bearing.resized.jpg', thumbUrl:'assets/img/products/thumbnails/best-seller/bearing.resized.jpg', name:'Bearing'},
			{image:'products/best-seller/connecting-rod.resized.jpg', thumbUrl:'assets/img/products/thumbnails/best-seller/connecting-rod.resized.jpg', name:'Connecting Rod'},
			{image:'products/best-seller/engine-valve.resized.jpg', thumbUrl:'assets/img/products/thumbnails/best-seller/engine-valve.resized.jpg', name:'Engine Valve'},
			{image:'products/best-seller/fuel-filter.resized.jpg', thumbUrl:'assets/img/products/thumbnails/best-seller/fuel-filter.resized.jpg', name:'Fuel Filter'},
			{image:'products/best-seller/gasket.resized.jpg', thumbUrl:'assets/img/products/thumbnails/best-seller/gasket.resized.jpg', name:'Gasket'},
		];
		// MGC Product
		vm.imgMP = [
			{image:'products/ball-bearing.resized.jpg', thumbUrl:'assets/img/products/thumbnails/mgc/ball-bearing.resized.jpg', name:'Ball Bearing'},
			{image:'products/battery.resized.jpg', thumbUrl:'assets/img/products/thumbnails/mgc/battery.resized.jpg', name:'Battery'},
			{image:'products/brake-pad.resized.jpg', thumbUrl:'assets/img/products/thumbnails/mgc/brake-pad.resized.jpg', name:'Brake Pad'},
			{image:'products/break-shoe.resized.jpg', thumbUrl:'assets/img/products/thumbnails/mgc/break-shoe.resized.jpg', name:'Break Shoe'},
			{image:'products/chain.resized.jpg', thumbUrl:'assets/img/products/thumbnails/mgc/chain.resized.jpg', name:'Chain'},
		];

		/**
		 * Lightbox
		 * @param index, category
		 */
		vm.openLightboxModal = function(index, cat) {

			var catImg = '';

			if (cat=="na") {
				catImg = vm.imgNA;
			} else if (cat=="bs") {
				catImg = vm.imgBS;
			} else {
				catImg = vm.imgMP;
			}

			Lightbox.openModal(catImg, index);

		};

	}


	function NewsEventsController() {
		
	}

})();