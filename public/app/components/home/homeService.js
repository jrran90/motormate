(function () {
	'use strict';

	angular
		.module('mgc')
		.factory('Post', Post);


	function Post ($http) {
		return {
			get: function () {
				return $http.get('api/post');
			}
		}
	}

})();