(function() {
	'use strict';

	angular
		.module('mgc')
		.config(config)
		.controller('ServiceController', ServiceController);


	function config(LightboxProvider) {
		LightboxProvider.templateUrl = '/app/components/services/serviceLightboxModal.html';

		LightboxProvider.getImageUrl = function (image) {
			return '/assets/img/'+ image.image;
		};

		LightboxProvider.getImageCaption = function (image) {
			return image.name;
		};
	}


	function ServiceController(Lightbox) {

		var vm = this;

		vm.images = [
			{image:'services/aluminum-welding.JPG', thumbUrl:'assets/img/services/aluminum-welding.JPG', name:'Aluminum Welding',description:'Motorcycle aluminium welding repairs. Aluminium welding repairs to engine casings. Aluminium welding repairs to components such as clutch and brake handle levers and brackets.'},
			{image:'services/bracket-fabrication.JPG', thumbUrl:'assets/img/services/bracket-fabrication.JPG', name:'Bracket Fabrication',description:'<strong>Accepts:</strong><br>Rear Footrest Bracket, XRM 125, XRM 110, Language Bracket XR200, Headlight Bracket, Front Disk Bracket Conversion, '},
			{image:'services/chassis-alignment.JPG', thumbUrl:'assets/img/services/chassis-alignment.JPG', name:'Chassis Alignment',description:'Chassis alignment of any kind of motorcycle, except Big bikes.'},
			{image:'services/chassis-conversion.JPG', thumbUrl:'assets/img/services/chassis-conversion.JPG', name:'Chassis Conversion',description:'<strong>Accepts:</strong><br>XR200 to CRF450, DT125 to YZ250, KMX 125 to KX250, SKYGO150 to KTM250-450, XL125 to CRF150'},
			{image:'services/crankshaft-press-alignment.JPG', thumbUrl:'assets/img/services/crankshaft-press-alignment.JPG', name:'Crankshaft Press Alignment',description:''},
			{image:'services/drill-type-milling machine.JPG', thumbUrl:'assets/img/services/drill-type-milling machine.JPG', name:'Drill Type Milling Machine', description:'Lathes are machines with a spindle that turns along with the workpiece, and a lathe knife or other cutting tool changes its shape. Lathes are primarily used to manufacture rotary parts, to lathe cylindrical surfaces, to drill openings and to cut threads (inner and outer). A lathe can be used to produce parts made of ferrous and non-ferrous metals, plastics and wood.'},
			{image:'services/foot-peg-conversion.JPG', thumbUrl:'assets/img/services/foot-peg-conversion.JPG', name:'Foot Peg Conversion',description:'<strong>Accepts:</strong><br>XRM125, XRM110, WAVE100, WAVE125, CRYPTON115, BRAVO115'},
			{image:'services/honing-machine.JPG', thumbUrl:'assets/img/services/honing-machine.JPG', name:'Honing Machine'},
			{image:'services/lathe-machine.JPG', thumbUrl:'assets/img/services/lathe-machine.JPG', name:'Lathe Machine',description:'Lathes are machines with a spindle that turns along with the workpiece, and a lathe knife or other cutting tool changes its shape. Lathes are primarily used to manufacture rotary parts, to lathe cylindrical surfaces, to drill openings and to cut threads (inner and outer). A lathe can be used to produce parts made of ferrous and non-ferrous metals, plastics and wood.'},
			{image:'services/rear-disc-brake-conversion.JPG', thumbUrl:'assets/img/services/rear-disc-brake-conversion.JPG', name:'Rear Disc Brake Conversion',description:'<strong>Accepts:</strong><br>XR200/125/150, Bravo115, Sniper classic 135, Wave125/100, DT125, Raider115'},
			{image:'services/rear-swing-arm-fabrication.JPG', thumbUrl:'assets/img/services/rear-swing-arm-fabrication.JPG', name:'Rear Swing Arm Fabrication',description:'<strong>Accepts:</strong><br>XRM125/110, DT125, XR200/125/150, KMX125, GRANDSTAR150, SKYGO150'},
			{image:'services/reboring-machine.JPG', thumbUrl:'assets/img/services/reboring-machine.JPG', name:'Reboring Machine',description:'Boring machines are used to mill, drill, bore, cut threads or face turn using a rotating tool, usually a cutter, drill, boring rod or milling head. Boring machines are used to drill closed and open openings in solid material, boring, reaming, threading, milling surfaces, etc. Drill bits, reamers, thread cutters, milling cutters and other tools are used to perform these operations. One type of boring machine is a horizontal boring machine with a horizontal spindle. Movement along individual axes needed for the work cycle is performed by a CNC control system.'},
		];

		vm.openLightboxModal = function (index,desc) {
			Lightbox.openModal(vm.images, index);
		};

	}

})();